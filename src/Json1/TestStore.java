package Json1;

import java.io.Serializable;
public class TestStore implements Serializable {
    private String answer;

    public TestStore() {
        answer = "";
    }

    public String getTestName() {
        return answer;
    }

    public void setTestName(String name) {
        answer = name;
    }

}